package ePubBook


import kotlin.system.exitProcess

/*
 * function main (repository in, repository out, fileNameFormat)
 *
 *  - repository in : the name of the directory in which all the ebooks will be found
 *    ready for processing and thus changing their name
 *  - repository out : the name of the directory in which the files will be saved resulting
 *    of the name processing
 *  - fileNameFormat : format of the name default is
 *    <Lastname> <Firstname> - <title> - <serie> - <volume>
 *
 */
fun main(args: Array<String>) {

    var ePubCtrl = ePubFileController()

    if ( ! ePubCtrl.getOptions(args)) {
        println("Bad arguments")
        exitProcess(1)
    }

    println("Processing from ${ePubCtrl.repoIn} to ${ePubCtrl.repoOut} using the format ${ePubCtrl.fileNameFormat} ...")

    ePubCtrl.run()

}