package ePubBook

import java.io.File


class ePubFileController {
    var repoIn  : String = "."
    var repoOut : String = "."
    var fileNameFormat : String = "%L, %F - %T - %S - Tome %n"

    fun getOptions(args: Array<String>) : Boolean {

        /* look at least for the 2 basic argument and why not the 3rd one */
        when(args.size) {
            2 -> {
                repoIn  = args[0]
                repoOut = args[1]
            }
            3 -> {
                repoIn  = args[0]
                repoOut = args[1]
                fileNameFormat = args[2]
            }
            else -> {
                return false
            }
        }

        return true
    }

    fun run() {
        /* browse the directory for epub files */
        File(repoIn).list().forEach {i ->
            if (i.substringAfterLast('.',"").equals("epub")) {
                /* we've got one of the ebooks */
                println("run : ${i}")
                var epub = ePubFile(i, repoIn)
                epub.getContent()
            }
        }
    }
}