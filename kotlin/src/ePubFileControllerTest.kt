package ePubBook

import org.junit.Assert.*
import org.junit.Test

class ePubFileControllerTest {
    
    @org.junit.Test
    fun getOptionsEmpty() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertFalse(ectrl.getOptions(arrayOf()))
        assertEquals(ectrl.repoIn,lrepoIn)
        assertEquals(ectrl.repoOut,lrepoout)
        assertEquals(ectrl.fileNameFormat,lfn)
    }
    @org.junit.Test
    fun getOptionsOneArg() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertFalse(ectrl.getOptions(arrayOf("1st Arg")))
        assertEquals(ectrl.repoIn,lrepoIn)
        assertEquals(ectrl.repoOut,lrepoout)
        assertEquals(ectrl.fileNameFormat,lfn)
    }
    @org.junit.Test
    fun getOptionsTwoArgs() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertTrue(ectrl.getOptions(arrayOf("1st Arg","2nd Arg")))
        assertEquals(ectrl.repoIn,"1st Arg")
        assertEquals(ectrl.repoOut,"2nd Arg")
        assertEquals(ectrl.fileNameFormat,lfn)
    }
    @org.junit.Test
    fun getOptionsThreeArgs() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertTrue(ectrl.getOptions(arrayOf("1st Arg","2nd Arg", "3rd Arg")))
        assertEquals(ectrl.repoIn,"1st Arg")
        assertEquals(ectrl.repoOut,"2nd Arg")
        assertEquals(ectrl.fileNameFormat,"3rd Arg")
    }
    @org.junit.Test
    fun getOptionsFourArgs() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertFalse(ectrl.getOptions(arrayOf("1st Arg","2nd Arg", "3rd Arg", "4th Arg")))
        assertEquals(ectrl.repoIn,lrepoIn)
        assertEquals(ectrl.repoOut,lrepoout)
        assertEquals(ectrl.fileNameFormat,lfn)
    }
    @org.junit.Test
    fun getOptionsTestData() {
        var ectrl = ePubFileController()
        val lrepoIn = ectrl.repoIn
        val lrepoout = ectrl.repoOut
        val lfn = ectrl.fileNameFormat
        assertTrue(ectrl.getOptions(arrayOf("./test/testData/in","./test/testData/out")))
        assertEquals(ectrl.repoIn,"./test/testData/in")
        assertEquals(ectrl.repoOut,"./test/testData/out")
        assertEquals(ectrl.fileNameFormat,lfn)
    }
}