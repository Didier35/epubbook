package ePubBook
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import java.io.File
import java.io.StringReader
import java.util.zip.ZipFile
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory


class ePubFile (_originalName : String, _pathName : String) {
    var originalName    : String = ""
    var pathName        : String = ""
    var contentPath     : String = ""
    var bookTitle       : String = ""
    var serieName       : String = ""
    var volumeNumber    : String = ""
    var bookAuthor      : MutableList<author> = mutableListOf<author>()

    inner class author {
        var authorFirstName : String = ""
        var authorLastName  : String = ""
    }

    init {
        this.originalName = _originalName
        this.pathName = _pathName
    }

    fun getContent() {
        this.getContentFileLocation()
        this.geteBookMetaData()
    }

    fun geteBookMetaData() {
        /* the content.opf file path has been extracted by getContentFileLocation() */
        /* it is supposed to be the full path from the root of the epub file */
        if (this.contentPath.isEmpty())
            return

        val xmlFile: File = File(pathName + "/" + this.contentPath)
        val dbFactory = DocumentBuilderFactory.newInstance()
        val dBuilder = dbFactory.newDocumentBuilder()
        val xmlInput = InputSource(StringReader(xmlFile.readText()))
        val doc = dBuilder.parse(xmlInput)
        val xpFactory = XPathFactory.newInstance()
        val xPath = xpFactory.newXPath()

        /* in the file "content.opf", in the <package><metadata> ...</></>, we have :     */
        /*      <dc:title> as the title of the book                                       */
        val xpath = "/package/metadata/*"
        val metadataNodeList : NodeList = xPath.evaluate(xpath, doc, XPathConstants.NODESET) as NodeList
        val metadataLength = metadataNodeList.length

        for (i in 0..metadataLength - 1) {
            val bookNode =  metadataNodeList.item(i)
            if (bookNode.nodeName.startsWith("dc:") && bookNode.firstChild.nodeValue != null)
                println("bookNode name : ${bookNode.nodeName} -> ${bookNode.firstChild.nodeValue}")

                if (bookNode.nodeName.contains("dc:title"))
                    this.bookTitle = bookNode.firstChild.nodeValue
                else if (bookNode.nodeName.contains("dc:creator")) {
                    var anAuthor = author()
                    anAuthor.authorFirstName = bookNode.firstChild.nodeValue.substringBefore(' ')
                    anAuthor.authorLastName = bookNode.firstChild.nodeValue.substringAfter(' ')
                    this.bookAuthor.add(anAuthor)
                }
        }


        /*      <dc:creator> as the author (FN LN) of the book  -> may be several lines   */
        /*       ????        as the serie of the story */
        /*       ????        as the volume number, which can appear in the format of x.y  */
    }

    fun getContentFileLocation() {
        /* open the epub file, should be a kind of zip file */
        val zipFile =  ZipFile(this.pathName + "/" + this.originalName)
        println(zipFile.getEntry("content.opf"))
        println(zipFile.getInputStream(zipFile.getEntry("content.opf")).read())
        val zipEntriesEnumeration  = zipFile.entries()
        while (zipEntriesEnumeration.hasMoreElements())
        {
            val currentEntry = zipEntriesEnumeration.nextElement();

            /* find the file "container.xml" in the "META-INF" directory */
 //           if (currentEntry.name.equals("META-INF/container.xml")) {
            if (currentEntry.name.endsWith("content.opf")) {
                //val xmlFile: File = File(pathName + "/" + currentEntry)
                val xmlFile: File = File( currentEntry.toString())
                val dbFactory = DocumentBuilderFactory.newInstance()
                val dBuilder = dbFactory.newDocumentBuilder()
                //val xmlInput = InputSource(StringReader(xmlFile.readText()))
                val xmlInput = zipFile.getInputStream(currentEntry)
                val doc = dBuilder.parse(xmlInput)
                val xpFactory = XPathFactory.newInstance()
                val xPath = xpFactory.newXPath()

                /* look for the entry rootfile pull-path. */
                val xpath = "/container/rootfiles/rootfile"
                val elementNodeList = xPath.evaluate(xpath, doc, XPathConstants.NODESET) as NodeList
                //val firstElement = elementNodeList.item(0)
                val elementNodeLength = elementNodeList.length

                for (i in 0..elementNodeLength - 1) {
                    val simpleElement =  elementNodeList.item(i)


                //    for (i in 0..firstElement.attributes.length - 1) {
                    val attribute = simpleElement.attributes.item(i)

                    if (attribute.nodeName.equals("full-path")) {
                        this.contentPath = attribute.nodeValue
                        println(attribute)
                    }
                }
            }
        }






//        val xlmFile: File = File(pathName + "/" + originalName)
//        val xmlDoc: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xlmFile)
//
//        xmlDoc.documentElement.normalize()
//
//        println("Root Node:" + xmlDoc.documentElement.nodeName)

//        val bookList: NodeList = xmlDoc.getElementsByTagName("book")

//        for(i in 0..bookList.length - 1)
//        {
//            var bookNode: Node = bookList.item(i)
//
//            if (bookNode.getNodeType() === Node.ELEMENT_NODE) {
//
//                val elem = bookNode as Element
//
//
//                val mMap = mutableMapOf<String, String>()
//
//
//                for(j in 0..elem.attributes.length - 1)
//                {
//                    mMap.putIfAbsent(elem.attributes.item(j).nodeName, elem.attributes.item(j).nodeValue)
//                }
//                println("Current Book : ${bookNode.nodeName} - $mMap")
//
//                println("Author: ${elem.getElementsByTagName("author").item(0).textContent}")
//                println("Title: ${elem.getElementsByTagName("title").item(0).textContent}")
//                println("Genre: ${elem.getElementsByTagName("genre").item(0).textContent}")
//                println("Price: ${elem.getElementsByTagName("price").item(0).textContent}")
//                println("publish_date: ${elem.getElementsByTagName("publish_date").item(0).textContent}")
//                println("description: ${elem.getElementsByTagName("description").item(0).textContent}")
//            }
//        }
    }
}


