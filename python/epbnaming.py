import sys
import ePubBook.ePubBookController as epbctrl
import epbFormatter.nameFormatter as epbf


# -----------------------------------------------------
#   Main function
# -----------------------------------------------------


if __name__ == "__main__":
    # first arg = repo Input, second arg = repo output
    if (len(sys.argv) < 3):
        sys.exit("Error epbmain.py repoInput repoOutput")

    lformatter = epbf.NameFormatter()
    ctrl = epbctrl.EPubFileController(sys.argv[1], sys.argv[2], lformatter)
    ctrl.parseInputDirectory()

    # print the list of file processed
    print ("--------------------")
    for e in ctrl.epbList:
        print ("Original file name : "+e.originalFileName)
        print ("Final file name    : "+e.finalFileName)
        print ("ebook title        : "+e.title)
        for a in e.authors:
            print ("ebook author       : "+a.firstName + " " + a.lastName)
        print ("Serie name         : "+e.serieTitle)
        print ("Serie Tome number  : "+e.serieIndex)
        ctrl.copyAndRenameFiles(e)
        print ("--------------------")

