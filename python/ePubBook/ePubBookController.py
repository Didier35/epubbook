import os, fnmatch, zipfile, shutil
import xml.etree.ElementTree as ET
import ePubBook


class EPubFileController():
    def __init__(self, rin, rout, lformatter):
        self.repoInput = rin
        self.repoOutput = rout
        self.epbList = []
        self.epbformatter = lformatter  # type is abstractformatter

    def toString(self):
        return self.repoInput + " " + self.repoOutput

    def extractContentOPFfromZip(self, epubfilename):
        # unzip the epub file to reach the content.opf file
        contentfile = None
        try:
            fzip = zipfile.ZipFile(epubfilename, 'r')
            for name in fzip.namelist():
                if name.endswith(".opf"):
                    break
            contentfile = fzip.open(name)
            #print ("OPF file : "+name)
        except:
            print("ERROR can't open content.opf in "+epubfilename)
            print(sys.exc_info()[0])
        
        return contentfile

    def processEPBFile(self,epbFile):
        print ("Processing file : "+epbFile)

        contentFile = self.extractContentOPFfromZip(self.repoInput + "/" + epbFile)

        if contentFile == None:
            # skip to next file
            return

        # create an ePubBook file and then fill the attribute from the xml parseing
        epb = ePubBook.EPubBook()
        epb.originalFileName = epbFile

        tree = ET.parse(contentFile)
        root = tree.getroot()

        for metadata in root.iter('*'):

            if ( metadata.tag.endswith("title")):
                epb.title = self.epbformatter.stripAccents( self.epbformatter.stripLineFeed(metadata.text))

            if ( metadata.tag.endswith("creator")):
                epb.processCreator( self.epbformatter.stripAccents( self.epbformatter.stripLineFeed(metadata.text)))

            if ( metadata.tag.endswith("meta")):
                if ("name" in metadata.attrib):
                    if (metadata.attrib['name'] == "calibre:series"):
                        # here we got the serie title
                        epb.serieTitle = self.epbformatter.stripAccents( self.epbformatter.stripLineFeed(metadata.attrib['content']))

                    if (metadata.attrib['name'] == "calibre:series_index"):
                        # here we got the serie index : "le tome" in french
                        epb.serieIndex = self.epbformatter.stripAccents( self.epbformatter.stripLineFeed(metadata.attrib['content']))

        epb.processFileName()

        # add this ebook in the list
        self.epbList.append(epb)
        contentFile.close()

    def parseInputDirectory(self):
        # parse the input directory for each epub file
        for entry in os.listdir(self.repoInput):
            if fnmatch.fnmatch(entry, "*.epub"):
                self.processEPBFile(entry)


    def copyAndRenameFiles(self, f):
        # copy file from repoin in repoout
        fin  = self.repoInput+"/"+f.originalFileName
        fout = self.repoOutput+"/"+f.finalFileName
        shutil.copy2( fin, fout)

