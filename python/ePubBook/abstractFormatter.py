import abc


class AbstractFormatter:
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def stripAccents(self, text):
        pass

    @abc.abstractmethod
    def stripLineFeed(self,text):
        pass

    @abc.abstractmethod
    def overallCleaning(self, text ):
        pass
