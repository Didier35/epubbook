import re
import author


class EPubBook:
    def __init__(self):
        self.originalFileName = ""
        self.finalFileName    = ""
        self.title            = ""
        self.authors          = []
        self.serieTitle       = ""
        self.serieIndex       = ""


    def processCreator(self, text):
        words = re.split('\W+',text)
        anAuthor = author.Author()
        anAuthor.firstName = words[0]
        if len(words) > 1:
            lname = words[1]
            for i in range(2,len(words)):
                lname = lname + " " + words[i]
            anAuthor.lastName = lname
        else:
            anAuthor.lastName = ""
        self.authors.append(anAuthor)

    def processFileName(self):
        # now can set the final file name based on the construction rule :
        # <author the first one name and firstname> - <serie if exists>, Tome <index if exists> - <title>.epub
        if (self.authors[0].lastName != ""):
            finalFileName = self.authors[0].lastName + " " + self.authors[0].firstName
        else:
            finalFileName = self.authors[0].firstName

        if (self.serieTitle != ""):
            finalFileName = finalFileName + " - " + self.serieTitle + ", Tome " + self.serieIndex

        finalFileName = finalFileName + " - " + self.title + ".epub"
        self.finalFileName = finalFileName


