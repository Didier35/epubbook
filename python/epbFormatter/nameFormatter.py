import unicodedata
import ePubBook.abstractFormatter as epbaf


class NameFormatter(epbaf.AbstractFormatter):

    def stripAccents(self, text):
        # convert plain text to utf-8
        u = text
        try:
            # u = unicode(text, "utf-8")
            u = unicodedata.normalize('NFD', u).encode('ascii', 'ignore')
        except:
            # just replace any non ascii character to avoir further issues
            # but better make these above solution work !!!
            s = u
            u = ""
            for c in s:
                xc = ord(c)
                if (xc < 128):
                    u = u + c
                else:
                    u = u + '_'

        # few additional cleaning :
        # remove any dangerous character which could endanger file manipulation
        # like '/', '.',...
        # Thus by now keep only : alpha, numeric, basic space, '-'
        s = u
        u = ""
        for c in s:
            if (c.isalnum() or c == ' ' or c == '-'):
                u = u + c
            else:
                u = u + ' '

        return (u)

    def stripLineFeed(self,text):
        return text.replace('\n','')

    def overallCleaning(self, text):
        ltext = self.stripAccents(text)
        ltext = self.stripLineFeed(ltext)
        return ltext
